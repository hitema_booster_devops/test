import Calculator from "./Calculator";

describe('Calculator', () => {
    let calculator: Calculator;

    beforeEach(() => {
        calculator = new Calculator();
    });

    it('should add two numbers', () => {
        expect(calculator.addition(1, 2)).toBe(3);
    });

    it('should subtract two numbers', () => {
        expect(calculator.subtraction(3, 2)).toBe(1);
    });

    it('should multiply two numbers', () => {
        expect(calculator.multiplication(2, 3)).toBe(6);
    });

    it('should divide two numbers', () => {
        expect(calculator.division(6, 3)).toBe(2);
    });

    it('should throw an error when dividing by zero', () => {
        expect(() => calculator.division(6, 0)).toThrowError('Cannot divide by zero');
    });

    it('should square a number', () => {
        expect(calculator.square(3)).toBe(9);
    });

    it('should square root a number', () => {
        expect(calculator.rootSquare(9)).toBe(3);
    });

    it('should throw an error when square rooting a negative number', () => {
        expect(() => calculator.rootSquare(-9)).toThrowError('Cannot square root a negative number');
    });
});